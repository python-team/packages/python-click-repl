click>=7.0
prompt_toolkit>=3.0.36

[testing]
pytest-cov>=4.0.0
pytest>=7.2.1
tox>=4.4.3
